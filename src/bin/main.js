import Vue from 'vue'
import App from './App'
import router from './Router'

import './FontAwesome'
import './Element'

import './assets/css/normalize.css'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App, },
    template: '<App/>',
})