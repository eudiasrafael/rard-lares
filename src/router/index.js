import PublicTemplate from '@/templates/Public'
import HomePage from '@/pages/General/Home'
import IconPage from '@/pages/General/Icon'
import UsrRegisterListPage from '@/pages/Register/usr-List'
import UsrRegisterAddPage from '@/pages/Register/usr-Add'
import AdmRegisterListPage from '@/pages/Register/adm-List'
import AdmRegisterAddPage from '@/pages/Register/adm-Add'

export default [
    {
        path: '',
        name: 'General',
        component: PublicTemplate,
        meta: {},
        redirect: { name: 'General.Home', },
        children: [
            {
                path: '',
                name: 'General.Home',
                component: HomePage,
                meta: {},
            },
            {
                path: 'icones',
                name: 'General.Icon',
                component: IconPage,
                meta: {},
            },
        ],
    },
    {
        path: 'usr/',
        name: 'Usr',
        component: PublicTemplate,
        meta: {},
        children: [
            {
                path: 'register',
                name: 'Usr.Register.List',
                component: UsrRegisterListPage,
                meta: {},
            },
            {
                path: 'register/add/',
                name: 'Usr.Register.Add',
                component: UsrRegisterAddPage,
                meta: {},
            },
        ],
    },
    {
        path: 'adm/',
        name: 'Adm',
        component: PublicTemplate,
        meta: {},
        children: [
            {
                path: 'register/',
                name: 'Adm.Register.List',
                component: AdmRegisterListPage,
                meta: {},
            },
            {
                path: 'register/add/',
                name: 'Adm.Register.Add',
                component: AdmRegisterAddPage,
                meta: {},
            },
        ],
    },
]